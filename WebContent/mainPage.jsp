<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.google.gson.Gson"%>
<%@ page
	import="com.sample.services.weather.domain.response.WeatherDataResponseDTO"%>
<%@ page import="com.sample.services.weather.domain.response.TimeDTO"%>
<%@ page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Weather Forcast</title>
</head>
<body>
	<div align="center" style="margin-top: 50px;">
		<h1>Weather Forecast</h1>
		<form action="WeatherServlet" method="POST">
			City name: <input type="text" name="city" /> <input type="submit"
				value="Submit" />
		</form>
	</div>


	<%
	String strResponse = (String) request.getAttribute("weather");
	if(null != strResponse && !strResponse.isEmpty()) {%>
	    
	<div align="center">
		        
		<table border="1" cellpadding="5">
			<%
				
				Gson gson = new Gson();
				WeatherDataResponseDTO data = gson.fromJson(strResponse,
						WeatherDataResponseDTO.class);

				List<TimeDTO> dataList = data.getForecast().getTime();
				%>
				<caption><h2><%=data.getName()%></h2></caption>
				<%
				for(TimeDTO dto : dataList)
				{
					%>	
				
				 <tr>
					                    
					<td>
					Date : <%=dto.getDay()%><br>
					<%=dto.getSymbol().getName()%><br>
					<%=dto.getClouds().getValue()%><br><br>
					
					Minimum Temperature : <%=dto.getTemperature().getMin()%><br>
					Maximum Temperature : <%=dto.getTemperature().getMax()%>
					</td>                     
					  
				</tr>	
					
					
					
			<%	} %>
			
  
		</table>
		    
	</div>
	<%	} else {%>
	<div align="center"> Invalid Request </div>
	<%	} %>
</body>
</html>
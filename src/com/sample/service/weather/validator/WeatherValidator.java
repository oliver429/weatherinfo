package com.sample.service.weather.validator;

import com.sample.services.weather.exception.BadInputException;

public class WeatherValidator {

	public static void validateData(String city) throws BadInputException
	{
		if(null == city || city.isEmpty())
		{
			throw new BadInputException();
		}
	}
}

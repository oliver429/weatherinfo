package com.sample.services.weather.util;

public class Constant {
	public static final String APPID = "a0d281fb51cb9aec95a1172244de5521";
	public static final String API = "api";
	public static final String OPEN_WEATHER_MAP = "openweathermap";
	public static final String org = "org";
	public static final String DATA = "data";
	public static final String VERSION = "2.5";
	public static final String WEATHER = "weather";
	public static final String CITY_TAG = "q";
	public static final String MODE = "mode";
	public static final String XML = "xml";
	public static final String EQUAL = "=";
	public static final String SLASH = "/";
	public static final String PERIOD = ".";
}

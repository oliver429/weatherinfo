package com.sample.services.weather;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.service.weather.validator.WeatherValidator;
import com.sample.services.weather.controller.WeatherController;
import com.sample.services.weather.exception.BadInputException;
import com.sample.services.weather.exception.ConnectionException;
import com.sample.services.weather.exception.WeatherNotFoundException;

/**
 * Servlet implementation class WeatherServlet
 */
@WebServlet("/WeatherServlet")
public class WeatherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public WeatherServlet() {
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		WeatherController weatherController = null;
		String currentWeatherResponseDTO = null;
		String city = (String) request.getAttribute("city");

		try {
			WeatherValidator.validateData(city);
			weatherController = new WeatherController();
			currentWeatherResponseDTO = weatherController
					.getWeatherByCity(city);
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");

			request.setAttribute("weather", currentWeatherResponseDTO);
		} catch (WeatherNotFoundException e) {
			request.setAttribute("weather", "");
		} catch (BadInputException e) {
			request.setAttribute("weather", "");

		} catch (ConnectionException e) {
			request.setAttribute("weather", "");
		}
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/mainPage.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		WeatherController weatherController = null;
		String currentWeatherResponseDTO = null;
		String city = (String) request.getParameter("city");

		try {
			WeatherValidator.validateData(city);
			weatherController = new WeatherController();
			currentWeatherResponseDTO = weatherController
					.getWeatherByCity(city);
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			request.setAttribute("weather", currentWeatherResponseDTO);

		} catch (WeatherNotFoundException e) {
			request.setAttribute("weather", "");
		} catch (BadInputException e) {
			request.setAttribute("weather", "");
		} catch (ConnectionException e) {
			request.setAttribute("weather", "");
		}
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/mainPage.jsp");
		dispatcher.forward(request, response);
	}

}

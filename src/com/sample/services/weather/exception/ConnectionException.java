package com.sample.services.weather.exception;

public class ConnectionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2498193300606164352L;

	public ConnectionException(String errorMessage, Throwable e) {
		super(errorMessage, e);
	}
}

package com.sample.services.weather.domain.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "clouds")
@XmlType(name = "clouds", propOrder = { "value", "all", "unit" })
public class CloudsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2361047688401693748L;

	private String value;
	private String all;
	private String unit;

	@XmlAttribute(name = "value")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@XmlAttribute(name = "all")
	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	@XmlAttribute(name = "unit")
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

}

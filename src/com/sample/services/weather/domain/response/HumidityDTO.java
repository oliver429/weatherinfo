package com.sample.services.weather.domain.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "humidity ")
@XmlType(name = "humidity ", propOrder = { "value", "unit" })
public class HumidityDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -161244673040492455L;

	private String value;
	private String unit;

	@XmlAttribute(name = "value")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@XmlAttribute(name = "unit")
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
}

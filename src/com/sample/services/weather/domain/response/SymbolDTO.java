package com.sample.services.weather.domain.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "symbol")
@XmlType(name = "symbol", propOrder = { "number", "name", "var" })
public class SymbolDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8103771537122418538L;

	private String number;
	private String name;
	private String var;

	@XmlAttribute(name = "number")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@XmlAttribute(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlAttribute(name = "var")
	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

}

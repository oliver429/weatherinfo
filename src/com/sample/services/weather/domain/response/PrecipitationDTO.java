package com.sample.services.weather.domain.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "precipitation")
@XmlType(name = "precipitation", propOrder = { "value", "unit", "type" })
public class PrecipitationDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2210428939177965517L;

	private String value;
	private String unit;
	private String type;

	@XmlAttribute(name = "value")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@XmlAttribute(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlAttribute(name = "unit")
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

}

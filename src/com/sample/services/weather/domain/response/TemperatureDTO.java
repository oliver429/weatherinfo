package com.sample.services.weather.domain.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "temperature")
@XmlType(name = "temperature", propOrder = { "day", "min", "max", "night",
		"eve", "morn" })
public class TemperatureDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2153904015350364926L;

	private String day;
	private String min;
	private String max;
	private String night;
	private String eve;
	private String morn;

	@XmlAttribute(name = "day")
	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	@XmlAttribute(name = "min")
	public String getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	@XmlAttribute(name = "max")
	public String getMax() {
		return max;
	}

	public void setMax(String max) {
		this.max = max;
	}

	@XmlAttribute(name = "night")
	public String getNight() {
		return night;
	}

	public void setNight(String night) {
		this.night = night;
	}

	@XmlAttribute(name = "eve")
	public String getEve() {
		return eve;
	}

	public void setEve(String eve) {
		this.eve = eve;
	}

	@XmlAttribute(name = "morn")
	public String getMorn() {
		return morn;
	}

	public void setMorn(String morn) {
		this.morn = morn;
	}

}

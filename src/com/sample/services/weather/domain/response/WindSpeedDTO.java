package com.sample.services.weather.domain.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "windSpeed")
@XmlType(name = "windSpeed", propOrder = { "mps", "name" })
public class WindSpeedDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8323144772349857231L;

	private String mps;
	private String name;

	@XmlAttribute(name = "mps")
	public String getMps() {
		return mps;
	}

	public void setMps(String mps) {
		this.mps = mps;
	}

	@XmlAttribute(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

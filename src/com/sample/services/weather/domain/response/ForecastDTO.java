package com.sample.services.weather.domain.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "forecast")
@XmlType(name = "forecast", propOrder = { "time" })
public class ForecastDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8054108239010892915L;

	List<TimeDTO> time = new ArrayList<TimeDTO>();


	@XmlElement(name = "time")
	public List<TimeDTO> getTime() {
		return time;
	}

	public void setTime(List<TimeDTO> forecast) {
		this.time = forecast;
	}

	public void addTime(TimeDTO time) {
		this.time.add(time);
	}

}

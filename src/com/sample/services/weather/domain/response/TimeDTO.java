package com.sample.services.weather.domain.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "time")
@XmlType(name = "time", propOrder = { "day", "symbol", "precipitation",
		"windDirection", "windSpeed", "temperature", "pressure", "humidity",
		"clouds" })
public class TimeDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7423942716818341461L;

	private String day;
	private SymbolDTO symbol;
	private PrecipitationDTO precipitation;
	private WindDirectionDTO windDirection;
	private WindSpeedDTO windSpeed;
	private TemperatureDTO temperature;
	private PressureDTO pressure;
	private HumidityDTO humidity;
	private CloudsDTO clouds;

	
	@XmlAttribute(name = "day")
	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	@XmlElement(name = "symbol")
	public SymbolDTO getSymbol() {
		return symbol;
	}

	public void setSymbol(SymbolDTO symbol) {
		this.symbol = symbol;
	}

	@XmlElement(name = "precipitation")
	public PrecipitationDTO getPrecipitation() {
		return precipitation;
	}

	public void setPrecipitation(PrecipitationDTO precipitation) {
		this.precipitation = precipitation;
	}

	@XmlElement(name = "windDirection")
	public WindDirectionDTO getWindDirection() {
		return windDirection;
	}

	public void setWindDirection(WindDirectionDTO windDirection) {
		this.windDirection = windDirection;
	}

	@XmlElement(name = "windSpeed")
	public WindSpeedDTO getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(WindSpeedDTO windSpeed) {
		this.windSpeed = windSpeed;
	}

	@XmlElement(name = "temperature")
	public TemperatureDTO getTemperature() {
		return temperature;
	}

	public void setTemperature(TemperatureDTO temperature) {
		this.temperature = temperature;
	}

	@XmlElement(name = "pressure")
	public PressureDTO getPressure() {
		return pressure;
	}

	public void setPressure(PressureDTO pressure) {
		this.pressure = pressure;
	}

	@XmlElement(name = "humidity")
	public HumidityDTO getHumidity() {
		return humidity;
	}

	public void setHumidity(HumidityDTO humidity) {
		this.humidity = humidity;
	}

	@XmlElement(name = "clouds")
	public CloudsDTO getClouds() {
		return clouds;
	}

	public void setClouds(CloudsDTO clouds) {
		this.clouds = clouds;
	}

}

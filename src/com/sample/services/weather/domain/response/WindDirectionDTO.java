package com.sample.services.weather.domain.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "windDirection")
@XmlType(name = "windDirection", propOrder = { "deg", "code", "name" })
public class WindDirectionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4403375332207530092L;

	private String deg;
	private String code;
	private String name;

	@XmlAttribute(name = "deg")
	public String getDeg() {
		return deg;
	}

	public void setDeg(String deg) {
		this.deg = deg;
	}

	@XmlAttribute(name = "code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@XmlAttribute(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

package com.sample.services.weather.domain.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "weatherdata")
@XmlType(name = "weatherdata", propOrder = { "name", "forecast" })
public class WeatherDataResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2153874608689712747L;

	private String name;
	private ForecastDTO forecast;

	@XmlElement(name = "forecast")
	public ForecastDTO getForecast() {
		return forecast;
	}

	public void setForecast(ForecastDTO forecast) {
		this.forecast = forecast;
	}

	@XmlElement(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

package com.sample.services.weather.controller;

import com.sample.services.weather.exception.BadInputException;
import com.sample.services.weather.exception.ConnectionException;
import com.sample.services.weather.exception.WeatherNotFoundException;


public interface IWeatherController {



	String getWeatherByCity(String city)
			throws WeatherNotFoundException, BadInputException, ConnectionException;
}


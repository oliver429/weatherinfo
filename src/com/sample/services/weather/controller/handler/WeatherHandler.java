package com.sample.services.weather.controller.handler;

import java.io.StringReader;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.sample.services.weather.controller.util.WebUtil;
import com.sample.services.weather.domain.response.WeatherDataResponseDTO;
import com.sample.services.weather.exception.ConnectionException;

public class WeatherHandler {

	public static WeatherDataResponseDTO getCurrentWeather(String aURL)
			throws ConnectionException {

		HttpURLConnection conn = null;
		conn = WebUtil.getConnection(aURL, WebUtil.GET);
		String strResponse = WebUtil.executeCall(conn);
		WeatherDataResponseDTO curr = null;

		String strManagedString = manageString(strResponse);

		if (null != strManagedString && !strManagedString.isEmpty()) {

			try {
				JAXBContext jaxbContext = JAXBContext
						.newInstance(WeatherDataResponseDTO.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext
						.createUnmarshaller();
				curr = (WeatherDataResponseDTO) jaxbUnmarshaller
						.unmarshal(new StringReader(strManagedString));
			} catch (JAXBException e) {

				e.printStackTrace();
			}
		}
		return curr;

	}

	private static String manageString(String strResponse) {

		List<String> myList = new ArrayList<String>(Arrays.asList(strResponse
				.split("<forecast>")));
		String newString = "";
		for (String str : myList) {
			if (str.contains("<time")) {
				newString = "<weatherdata><forecast>" + str;
			}
		}

		return newString;
	}
}

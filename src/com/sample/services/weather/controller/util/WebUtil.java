package com.sample.services.weather.controller.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import com.sample.services.weather.exception.ConnectionException;

public final class WebUtil {

	public static final String GET = "GET";
	private static final Logger LOGGER = Logger.getLogger(WebUtil.class
			.toString());
	private static final String CLASS = "WebUtil.";

	/**
	 * Constructor
	 */
	private WebUtil() {
	}

	public static HttpURLConnection getConnection(String aUrlString,
			String aRequestMethod) {

		final String METHOD = CLASS + "getConnection - ";
		HttpURLConnection lConnection = null;

		try {
			URL oURL = new URL(aUrlString);
			lConnection = (HttpURLConnection) oURL.openConnection();
			lConnection.setDoOutput(true);
			lConnection.setRequestMethod(aRequestMethod);

			int statusCode = lConnection.getResponseCode();

			if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {

			} else if (!isValidStatusCodes(statusCode)) {

			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return lConnection;

	}

	public static String executeCall(HttpURLConnection aConnection)
			throws ConnectionException {
		final String METHOD = CLASS + "executeCall - ";
		StringBuilder lStringBuilder = new StringBuilder("");
		try {
			BufferedReader lBufferedReader = new BufferedReader(
					new InputStreamReader(aConnection.getInputStream()));
			String output = "";

			while ((output = lBufferedReader.readLine()) != null) {
				lStringBuilder.append(output);
			}

			aConnection.disconnect();
			lBufferedReader.close();
		} catch (IOException e) {
			System.out.println(e);
			throw new ConnectionException(e.getCause().getMessage(), e);
		}

		String lReturn = lStringBuilder.toString();

		return lReturn;

	}

	/**
	 * Return true if <code>aStatusCode</code> is a valid HTTP status code, else
	 * false
	 * 
	 * @param aStatusCode
	 * @return
	 */
	private static boolean isValidStatusCodes(int aStatusCode) {
		int[] lValidStatusCodes = new int[] { HttpURLConnection.HTTP_OK,
				HttpURLConnection.HTTP_CREATED };

		for (int lCode : lValidStatusCodes) {
			if (aStatusCode == lCode) {
				return true;
			}
		}

		return false;
	}
}
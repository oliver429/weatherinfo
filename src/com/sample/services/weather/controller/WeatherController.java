package com.sample.services.weather.controller;

import com.sample.services.weather.exception.BadInputException;
import com.sample.services.weather.exception.ConnectionException;
import com.sample.services.weather.exception.WeatherNotFoundException;
import com.sample.services.weather.service.WeatherService;

public class WeatherController implements IWeatherController {

	@Override
	public String getWeatherByCity(String city)
			throws WeatherNotFoundException, BadInputException,
			ConnectionException {
		WeatherService weatherService = new WeatherService();
		String currentWeather = weatherService.getWeatherByCity(city);
		return currentWeather;
	}

}

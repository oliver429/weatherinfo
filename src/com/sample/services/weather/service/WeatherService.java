package com.sample.services.weather.service;

import com.google.gson.Gson;
import com.sample.services.weather.controller.handler.WeatherHandler;
import com.sample.services.weather.domain.response.WeatherDataResponseDTO;
import com.sample.services.weather.exception.BadInputException;
import com.sample.services.weather.exception.ConnectionException;
import com.sample.services.weather.exception.WeatherNotFoundException;

public class WeatherService implements IWeatherService {

	@Override
	public String getWeatherByCity(String city) throws BadInputException,
			WeatherNotFoundException, ConnectionException {
		
		WeatherDataResponseDTO currWeather = WeatherHandler
				.getCurrentWeather(getDafaultUrl(city));
		currWeather.setName(city);
		

		String weather = null;
		Gson gson = new Gson();
		weather = gson.toJson(currWeather);
		return weather;
	}

	private String getDafaultUrl(String city) {
		return "http://api.openweathermap.org/data/2.5/forecast/daily?q="
				+ city
				+ "&mode=xml&units=metric&cnt=6&APPID=a0d281fb51cb9aec95a1172244de5521";
	}

}

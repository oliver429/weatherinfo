package com.sample.services.weather.service;

import com.sample.services.weather.exception.BadInputException;
import com.sample.services.weather.exception.ConnectionException;
import com.sample.services.weather.exception.WeatherNotFoundException;

public interface IWeatherService {
	String getWeatherByCity(String city)
			throws BadInputException, WeatherNotFoundException,
			ConnectionException;
}
